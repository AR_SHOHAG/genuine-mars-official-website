<?php
include_once 'includes/header.php';
?>

    <body>
    
        <header class="clearfix">
        
            <!-- Start Top Bar -->
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="top-bar">
                            <div class="row">
                                    
                                <div class="col-md-6">
                                    <!-- Start Contact Info -->
                                    <ul class="contact-details">
                                        <li><a href="#"><i class="fa fa-phone"></i> +880 1724338212</a>
                                        </li>
                                        <li><a href="#"><i class="fa fa-envelope-o"></i>support@genuinemars.com</a>
                                        </li>
                                    </ul>
                                    <!-- End Contact Info -->
                                </div><!-- .col-md-6 -->
                                
                                <div class="col-md-6">
                                    <!-- Start Social Links -->
                                    <ul class="social-list">
                                        <li>
                                            <a href="https://www.facebook.com/genuinemars/"><i class="fa fa-rss"></i></a>
                                        </li>
                                        <li>
                                            <a href="https://www.facebook.com/genuinemars/"><i class="fa fa-facebook"></i></a>
                                        </li>
                                        <li>
                                            <a href="https://www.facebook.com/genuinemars/"><i class="fa fa-twitter"></i></a>
                                        </li>
                                        <li>
                                            <a href="https://www.facebook.com/genuinemars/"><i class="fa fa-google-plus"></i></a>
                                        </li>
                                        <li>
                                            <a href="https://www.facebook.com/genuinemars/"><i class="fa fa-youtube"></i></a>
                                        </li>
                                        <li>
                                            <a href="https://www.facebook.com/genuinemars/"><i class="fa fa-linkedin"></i></a>
                                        </li>
                                        <li>
                                            <a href="https://www.facebook.com/genuinemars/"><i class="fa fa-flickr"></i></a>
                                        </li>
                                        <li>
                                            <a href="https://www.facebook.com/genuinemars/"><i class="fa fa-vimeo-square"></i></a>
                                        </li>
                                        <li>
                                            <a href="https://www.facebook.com/genuinemars/"><i class="fa fa-skype"></i></a>
                                        </li>
                                    </ul>
                                    <!-- End Social Links -->
                                </div><!-- .col-md-6 -->
                            </div>
                                
                                
                        </div>
                    </div>                        

                </div><!-- .row -->
            </div><!-- .container -->
            <!-- End Top Bar -->
        
            <!-- Start  Logo & Naviagtion  -->
            <div class="navbar navbar-default navbar-top">
                <div class="container">
                    <div class="navbar-header">
                        <!-- Stat Toggle Nav Link For Mobiles -->
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <i class="fa fa-bars"></i>
                        </button>
                        <!-- End Toggle Nav Link For Mobiles -->
                        <a class="navbar-brand" href="index.php">GENUINE MARS</a>
                    </div>
                    <div class="navbar-collapse collapse">
                        
                        <!-- Start Navigation List -->
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a href="index.php">Home</a>
                            </li>
                            <li>
                                <a href="view/front/about.php">About Us</a>
                            </li>
                            <li>
                                <a href="view/front/service.php">Service</a>
                            </li>
                            <li>
                                <a href="view/front/portfolio.php">Portfolio</a>
                            </li>
                            <li>
                                <a href="view/front/blog.php">Blog</a>
                                <ul class="dropdown">
                                    <li>
                                        <a href="view/front/blog-item.php">Item Page</a>
                                    </li>
                                </ul>
                            </li>
                            <li><a class="active" href="view/front/contact.php">Contact</a>
                            </li>
                        </ul>
                        <!-- End Navigation List -->
                    </div>
                </div>
            </div>
            <!-- End Header Logo & Naviagtion -->
            
        </header>
        
        
        <!-- Start Header Section -->
        <div class="page-header">
            <div class="overlay">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h1>Contact with Us</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Header Section -->
        
        
        
        
        
        

        
        <!-- Start Contact Us Section -->
    <section id="contact" class="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title text-center wow fadeInDown" data-wow-duration="2s" data-wow-delay="50ms">
                        <h2>Contact With Us</h2>
                        <p>Thanks for being with us. Please fill up the form and send. We will reply soon.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <form name="sentMessage" id="contactForm" novalidate>
                        <div class="row">
                            <div class="col-md-6 wow fadeInLeft" data-wow-duration="2s" data-wow-delay="600ms">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Your Name *" id="name" required data-validation-required-message="Please enter your name.">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control" placeholder="Your Email *" id="email" required data-validation-required-message="Please enter your email address.">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input type="tel" class="form-control" placeholder="Your Phone *" id="phone" required data-validation-required-message="Please enter your phone number.">
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="col-md-6 wow fadeInRight" data-wow-duration="2s" data-wow-delay="600ms">
                                <div class="form-group">
                                    <textarea class="form-control" placeholder="Your Message *" id="message" required data-validation-required-message="Please enter a message."></textarea>
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-lg-12 text-center wow zoomIn" data-wow-duration="1s" data-wow-delay="600ms">
                                <div id="success"></div>
                                <button type="submit" class="btn btn-primary">Send Message</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        
    </section>

    <div class="google-map">
        <div id="map" data-position-latitude="48.858370" data-position-longitude="2.294481"></div>
    </div>



<?php
include_once 'includes/footer.php';
?>