<?php
include_once 'includes/header.php';
?>

    <body>

<header class="clearfix">

    <!-- Start Top Bar -->
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="top-bar">
                    <div class="row">

                        <div class="col-md-6">
                            <!-- Start Contact Info -->
                            <ul class="contact-details">
                                <li><a href="#"><i class="fa fa-phone"></i> +880 1724338212</a>
                                </li>
                                <li><a href="#"><i class="fa fa-envelope-o"></i>support@genuinemars.com</a>
                                </li>
                            </ul>
                            <!-- End Contact Info -->
                        </div><!-- .col-md-6 -->

                        <div class="col-md-6">
                            <!-- Start Social Links -->
                            <ul class="social-list">
                                <li>
                                    <a href="https://www.facebook.com/genuinemars/"><i class="fa fa-rss"></i></a>
                                </li>
                                <li>
                                    <a href="https://www.facebook.com/genuinemars/"><i class="fa fa-facebook"></i></a>
                                </li>
                                <li>
                                    <a href="https://www.facebook.com/genuinemars/"><i class="fa fa-twitter"></i></a>
                                </li>
                                <li>
                                    <a href="https://www.facebook.com/genuinemars/"><i class="fa fa-google-plus"></i></a>
                                </li>
                                <li>
                                    <a href="https://www.facebook.com/genuinemars/"><i class="fa fa-youtube"></i></a>
                                </li>
                                <li>
                                    <a href="https://www.facebook.com/genuinemars/"><i class="fa fa-linkedin"></i></a>
                                </li>
                                <li>
                                    <a href="https://www.facebook.com/genuinemars/"><i class="fa fa-flickr"></i></a>
                                </li>
                                <li>
                                    <a href="https://www.facebook.com/genuinemars/"><i class="fa fa-vimeo-square"></i></a>
                                </li>
                                <li>
                                    <a href="https://www.facebook.com/genuinemars/"><i class="fa fa-skype"></i></a>
                                </li>
                            </ul>
                            <!-- End Social Links -->
                        </div><!-- .col-md-6 -->
                    </div>


                </div>
            </div>

        </div><!-- .row -->
    </div><!-- .container -->
    <!-- End Top Bar -->

    <!-- Start  Logo & Naviagtion  -->
    <div class="navbar navbar-default navbar-top">
        <div class="container">
            <div class="navbar-header">
                <!-- Stat Toggle Nav Link For Mobiles -->
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <i class="fa fa-bars"></i>
                </button>
                <!-- End Toggle Nav Link For Mobiles -->
                <a class="navbar-brand" href="index.php">GENUINE MARS</a>
            </div>
            <div class="navbar-collapse collapse">

                <!-- Start Navigation List -->
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="index.php">Home</a>
                    </li>
                    <li>
                        <a class="active" href="view/front/about.php">About Us</a>
                    </li>
                    <li>
                        <a href="view/front/service.php">Service</a>
                    </li>
                    <li>
                        <a href="view/front/portfolio.php">Portfolio</a>
                    </li>
                    <li>
                        <a href="view/front/blog.php">Blog</a>
                        <ul class="dropdown">
                            <li>
                                <a href="view/front/blog-item.php">Item Page</a>
                            </li>
                        </ul>
                    </li>
                    <li><a href="view/front/contact.php">Contact</a>
                    </li>
                </ul>
                <!-- End Navigation List -->
            </div>
        </div>
    </div>
    <!-- End Header Logo & Naviagtion -->

</header>


<!-- Start Header Section -->
<div class="page-header">
    <div class="overlay">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>About Us</h1>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Header Section -->


<!-- Start About Us Section -->
<section id="about-section" class="about-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title text-center wow fadeInDown" data-wow-duration="2s" data-wow-delay="50ms">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5">
                <div class="about-img">
                    <img src="assets/front/images/ceo.jpg" class="img-responsive" alt="About images">
                    <!--<div class="head-text">
                        <p>Genuine MARS is founded to create an invincible originality in IT/Software business in the web world.</p>
                        <span>CEO,  Md. Arifur Rahman</span>
                    </div>-->
                </div>
                <div class="head-text">
                    <p>"Genuine MARS is founded to create an invincible originality in IT/Software business in the web world."</p>
                    <span>CEO,  Md. Arifur Rahman</span>
                </div>
            </div>
            <div class="col-md-7">
                <div class="about-text">
                    <p>Genuine mars is a highly reputed software company that has been praised by the professional customers all over the world. We do things what we would love to do, and that makes us distinct from others. Besides we have the most highly skilled visionary creative developers those are deeply compatible with recent technologies.</p>
                    <p>We work on some sort of activities. At present we design and develop high quality responsive websites by the touch of our highly professional web team. We also develop wordpess template theme, plugins and sell them in our own marketplace as well as the popular marketplaces in the world. Besides we provide domain hosting for websites with very excellent customer service.</p>
                </div>

                <div class="about-list">
                    <h4>Some important Feature</h4>
                    <ul>
                        <li><i class="fa fa-check-square"></i>We are always here to help you any time you need.</li>
                        <li><i class="fa fa-check-square"></i>We always value our clients’ precious time so we never compromise with delivery time.</li>
                        <li><i class="fa fa-check-square"></i>We always make exclusive and attractive design.</li>
                        <li><i class="fa fa-check-square"></i>To make our clients’ tension free we always focus on highest security of our services.</li>
                        <li><i class="fa fa-check-square"></i>We always develop fully responsive and Mobile friendly website.</li>
                        <li><i class="fa fa-check-square"></i>Ready to make clients fully satisfied.</li>
                    </ul>

                    <h4>More Feature</h4>
                    <ul>
                        <li><i class="fa fa-check-square"></i>Lifetime Support.</li>
                        <li><i class="fa fa-check-square"></i>In Time Delivery</li>
                        <li><i class="fa fa-check-square"></i>Reasonable price.</li>
                        <li><i class="fa fa-check-square"></i>No compromise with quality.</li>
                    </ul>
                </div>

            </div>



        </div>
    </div>
</section>

<!-- Start About-section 2 -->
<section id="about-section-2">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="about-text">
                    <h2 style="margin-left: 205px;">Our Skills</h2>
                </div>

                <div class="skill-shortcode">

                    <!-- Progress Bar -->
                    <div class="skill">
                        <p>PHP</p>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar"  data-percentage="98">
                                <span class="progress-bar-span" >98%</span>
                                <span class="sr-only">60% Complete</span>
                            </div>
                        </div>
                    </div>

                    <!-- Progress Bar -->
                    <div class="skill">
                        <p>Laravel</p>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar"  data-percentage="90">
                                <span class="progress-bar-span" >90%</span>
                                <span class="sr-only">90% Complete</span>
                            </div>
                        </div>
                    </div>

                    <!-- Progress Bar -->
                    <div class="skill">
                        <p>Bootstrap</p>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar"  data-percentage="95">
                                <span class="progress-bar-span" >95%</span>
                                <span class="sr-only">95% Complete</span>
                            </div>
                        </div>
                    </div>

                    <!-- Progress Bar -->
                    <div class="skill">
                        <p>Ruby on Rails</p>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar"  data-percentage="85">
                                <span class="progress-bar-span" >85%</span>
                                <span class="sr-only">85% Complete</span>
                            </div>
                        </div>
                    </div>

                    <!-- Progress Bar -->
                    <div class="skill">
                        <p>Wordpress</p>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar"  data-percentage="90">
                                <span class="progress-bar-span" >90%</span>
                                <span class="sr-only">90% Complete</span>
                            </div>
                        </div>
                    </div>

                    <!-- Progress Bar -->
                    <div class="skill">
                        <p>Magento</p>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar"  data-percentage="80">
                                <span class="progress-bar-span" >80%</span>
                                <span class="sr-only">80% Complete</span>
                            </div>
                        </div>
                    </div>

                    <!-- Progress Bar -->
                    <div class="skill">
                        <p>JavaScript</p>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar"  data-percentage="97">
                                <span class="progress-bar-span" >97%</span>
                                <span class="sr-only">97% Complete</span>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

            <div class="col-md-6 wow fadeInLeft" data-wow-duration="2s" data-wow-delay="300ms">
                <h2 style="margin-left: 239px;">More</h2>
                <?php echo '<br><br>';?>
                <!-- Start Accordion Section -->
                <div class="panel-group" id="accordion">

                    <!-- Start Accordion 1 -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse-1">
                                    <i class="fa fa-angle-left control-icon"></i> Who We are
                                </a>
                            </h4>
                        </div>
                        <div id="collapse-1" class="panel-collapse collapse in">
                            <div class="panel-body">We are a group of visionary young people, we dream about a great peaceful, prosperous and technological developed world.</div>								</div>
                    </div>
                    <!-- End Accordion 1 -->

                    <!-- Start Accordion 2 -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse-2" class="collapsed">
                                    <i class="fa fa-angle-left control-icon"></i> What we do
                                </a>
                            </h4>
                        </div>
                        <div id="collapse-2" class="panel-collapse collapse">
                            <div class="panel-body">We design and develop website from scratch or psd or based on any idea. We use the latest web technologies and techniques</div>								</div>
                    </div>
                    <!-- End Accordion 2 -->

                    <!-- Start Accordion 3 -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse-3" class="collapsed">
                                    <i class="fa fa-angle-left control-icon"></i> Our Goal
                                </a>
                            </h4>
                        </div>
                        <div id="collapse-3" class="panel-collapse collapse">
                            <div class="panel-body">We believe in quality, our goal is to make the clients 100% satisfied so that we can help them in their very business.</div>								</div>
                    </div>
                    <!-- End Accordion 3 -->

                    <!-- Start Accordion 4 -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse-4" class="collapsed">
                                    <i class="fa fa-angle-left control-icon"></i> Why Choose Us ?
                                </a>
                            </h4>
                        </div>
                        <div id="collapse-4" class="panel-collapse collapse">
                            <div class="panel-body">Only we provide the best quality products in the cheapest price in the required time. This makes us different from others and that is we're the best option for you.</div>
                        </div>
                    </div>
                    <!-- End Accordion 4 -->

                    <!-- Start Accordion 5 -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse-5" class="collapsed">
                                    <i class="fa fa-angle-left control-icon"></i> Our Great Support
                                </a>
                            </h4>
                        </div>
                        <div id="collapse-5" class="panel-collapse collapse">
                            <div class="panel-body">Hard work and keep trying are our best supports and the happy and permanent clients are our inspiration.</div>
                        </div>
                    </div>
                    <!-- End Accordion 5 -->

                </div>
                <!-- End Accordion section -->

            </div><!--/.col-md-6 -->
        </div>
    </div>
</section>
<!-- Start About-section 2 -->


<!-- Start Call to Action Section -->
<section class="call-to-action">
    <div class="container">
        <div class="row">
            <div class="col-md-12 wow zoomIn" data-wow-duration="2s" data-wow-delay="300ms">
                <p>With  years of experience, being a part of an awesome team <br>we've created a creative  and aesthetic environment that <br>breeds innovative works. Please take a look.</p>
            </div>
        </div>
    </div>
</section>
<!-- End Call to Action Section -->




<!-- Start Team Member Section -->
<section id="team-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title text-center wow fadeInDown" data-wow-duration="2s" data-wow-delay="50ms">
                    <h2>Our Team</h2>
                    <p>Meet with the awesome team members</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 wow fadeInLeft" data-wow-duration="2s" data-wow-delay="300ms">
                <div class="team-member">
                    <img src="assets/front/images/team/teamm.jpg" class="img-responsive" alt="">
                    <div class="team-details">
                        <h4>Md. Arifur Rahman</h4>
                        <p>Co-Founder & CEO</p>
                        <ul>
                            <li><a href="https://www.facebook.com/profile.php?id=100007331657504"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                            <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div><!-- /.col-md-3 -->
            <div class="col-md-3 wow fadeInLeft" data-wow-duration="2s" data-wow-delay="600ms">
                <div class="team-member">
                    <img src="assets/front/images/team/team2.jpg" class="img-responsive" alt="">
                    <div class="team-details">
                        <h4>Al-Mahfuz</h4>
                        <p>Co-Founder & CEO</p>
                        <ul>
                            <li><a href="https://www.facebook.com/mahfuz380?ref=br_rs"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                            <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div><!-- /.col-md-3 -->
            <div class="col-md-3 wow fadeInLeft" data-wow-duration="2s" data-wow-delay="900ms">
                <div class="team-member">
                    <img src="assets/front/images/team/team3.jpg" class="img-responsive" alt="">
                    <div class="team-details">
                        <h4>Md. Fayjur Rahmane</h4>
                        <p>Co-Founder & CEO</p>
                        <ul>
                            <li><a href="https://www.facebook.com/fayjur121"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                            <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div><!-- /.col-md-3 -->
            <div class="col-md-3 wow fadeInLeft" data-wow-duration="2s" data-wow-delay="1200ms">
                <div class="team-member">
                    <img src="assets/front/images/team/team4.jpg" class="img-responsive" alt="">
                    <div class="team-details">
                        <h4>Sakib Sadman</h4>
                        <p>Graphic Designer</p>
                        <ul>
                            <li><a href="https://www.facebook.com/sakib.sadman.547"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                            <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div><!-- /.col-md-3 -->



        </div>

        <?php echo '<br>';?>

        <div class="row">
            <div class="col-md-3 wow fadeInLeft" data-wow-duration="2s" data-wow-delay="300ms">
                <div class="team-member">
                    <img src="assets/front/images/team/team5.jpg" class="img-responsive" alt="">
                    <div class="team-details">
                        <h4>Sohel Rana</h4>
                        <p>Web Designer</p>
                        <ul>
                            <li><a href="https://www.facebook.com/profile.php?id=100007331657504"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                            <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div><!-- /.col-md-3 -->
            <div class="col-md-3 wow fadeInLeft" data-wow-duration="2s" data-wow-delay="600ms">
                <div class="team-member">
                    <img src="assets/front/images/team/team6.jpg" class="img-responsive" alt="">
                    <div class="team-details">
                        <h4>Md. Shoriful Islam</h4>
                        <p>Graphic Designer</p>
                        <ul>
                            <li><a href="https://www.facebook.com/mahfuz380?ref=br_rs"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                            <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div><!-- /.col-md-3 -->
            <div class="col-md-3 wow fadeInLeft" data-wow-duration="2s" data-wow-delay="900ms">
                <div class="team-member">
                    <img src="assets/front/images/team/team7.jpg" class="img-responsive" alt="">
                    <div class="team-details">
                        <h4>Wasiq Shibly</h4>
                        <p>Web Designer</p>
                        <ul>
                            <li><a href="https://www.facebook.com/fayjur121"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                            <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div><!-- /.col-md-3 -->
            <div class="col-md-3 wow fadeInLeft" data-wow-duration="2s" data-wow-delay="1200ms">
                <div class="team-member">
                    <img src="assets/front/images/team/team8.jpg" class="img-responsive" alt="">
                    <div class="team-details">
                        <h4>Zakarea Khandakar</h4>
                        <p>Marketing Officer</p>
                        <ul>
                            <li><a href="https://www.facebook.com/sakib.sadman.547"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                            <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div><!-- /.col-md-3 -->



        </div>
    </div>
</section>
<!-- End Team Member Section -->





<!-- Start Client Section -->
<div id="client-section" style="border-top: 1px solid #f1f1f1;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="client-box">
                    <ul class="client-list">
                        <li><a href="#"><img src="assets/front/images/clients/client1.png" class="img-responsive" alt="Clients Logo"></a></li>
                        <li><a href="#"><img src="assets/front/images/clients/client2.png" class="img-responsive" alt="Clients Logo"></a></li>
                        <li><a href="#"><img src="assets/front/images/clients/client3.png" class="img-responsive" alt="Clients Logo"></a></li>
                        <li><a href="#"><img src="assets/front/images/clients/client4.png" class="img-responsive" alt="Clients Logo"></a></li>
                        <li><a href="#"><img src="assets/front/images/clients/client5.png" class="img-responsive" alt="Clients Logo"></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Client Section -->



<?php
include_once 'includes/footer.php';
?>