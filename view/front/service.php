<?php
include_once 'includes/header.php';
?>

    <body>
    
        <header class="clearfix">
        
            <!-- Start Top Bar -->
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="top-bar">
                            <div class="row">
                                    
                                <div class="col-md-6">
                                    <!-- Start Contact Info -->
                                    <ul class="contact-details">
                                        <li><a href="#"><i class="fa fa-phone"></i> +880 1724338212</a>
                                        </li>
                                        <li><a href="#"><i class="fa fa-envelope-o"></i>support@genuinemars.com</a>
                                        </li>
                                    </ul>
                                    <!-- End Contact Info -->
                                </div><!-- .col-md-6 -->
                                
                                <div class="col-md-6">
                                    <!-- Start Social Links -->
                                    <ul class="social-list">
                                        <li>
                                            <a href="https://www.facebook.com/genuinemars/"><i class="fa fa-rss"></i></a>
                                        </li>
                                        <li>
                                            <a href="https://www.facebook.com/genuinemars/"><i class="fa fa-facebook"></i></a>
                                        </li>
                                        <li>
                                            <a href="https://www.facebook.com/genuinemars/"><i class="fa fa-twitter"></i></a>
                                        </li>
                                        <li>
                                            <a href="https://www.facebook.com/genuinemars/"><i class="fa fa-google-plus"></i></a>
                                        </li>
                                        <li>
                                            <a href="https://www.facebook.com/genuinemars/"><i class="fa fa-youtube"></i></a>
                                        </li>
                                        <li>
                                            <a href="https://www.facebook.com/genuinemars/"><i class="fa fa-linkedin"></i></a>
                                        </li>
                                        <li>
                                            <a href="https://www.facebook.com/genuinemars/"><i class="fa fa-flickr"></i></a>
                                        </li>
                                        <li>
                                            <a href="https://www.facebook.com/genuinemars/"><i class="fa fa-vimeo-square"></i></a>
                                        </li>
                                        <li>
                                            <a href="https://www.facebook.com/genuinemars/"><i class="fa fa-skype"></i></a>
                                        </li>
                                    </ul>
                                    <!-- End Social Links -->
                                </div><!-- .col-md-6 -->
                            </div>
                                
                                
                        </div>
                    </div>                        

                </div><!-- .row -->
            </div><!-- .container -->
            <!-- End Top Bar -->
        
            <!-- Start  Logo & Naviagtion  -->
            <div class="navbar navbar-default navbar-top">
                <div class="container">
                    <div class="navbar-header">
                        <!-- Stat Toggle Nav Link For Mobiles -->
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <i class="fa fa-bars"></i>
                        </button>
                        <!-- End Toggle Nav Link For Mobiles -->
                        <a class="navbar-brand" href="index.php">GENUINE MRAS</a>
                    </div>
                    <div class="navbar-collapse collapse">
                        
                        <!-- Start Navigation List -->
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a href="index.php">Home</a>
                            </li>
                            <li>
                                <a href="view/front/about.php">About Us</a>
                            </li>
                            <li>
                                <a class="active" href="service.php">Service</a>
                            </li>
                            <li>
                                <a href="view/front/portfolio.php">Portfolio</a>
                            </li>
                            <li>
                                <a href="view/front/blog.php">Blog</a>
                                <ul class="dropdown">
                                    <li>
                                        <a href="view/front/blog-item.php">Item Page</a>
                                    </li>
                                </ul>
                            </li>
                            <li><a href="view/front/contact.php">Contact</a>
                            </li>
                        </ul>
                        <!-- End Navigation List -->
                    </div>
                </div>
            </div>
            <!-- End Header Logo & Naviagtion -->
            
        </header>
        
        
        <!-- Start Header Section -->
        <div class="page-header">
            <div class="overlay">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h1>Our Services</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Header Section -->
        
        
        
        
        <!-- Start Service Section -->
        <section id="service-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section-title text-center wow fadeInDown" data-wow-duration="2s" data-wow-delay="50ms">
                            <h2>Our Services</h2>
                            <p>We are the only one who provide the best quality <br>products at the cheapest price in the global market. <br>We believe our price strategy is the best you've ever seen.</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="services-post">
                            <a href="#"><i class="fa fa-skyatlas"></i></a>
                            <h2>RESPONSIVE DESIGN</h2>
                            <p>Donec odio. Quisque volutpat mattis eros. Nullam malesuada </p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="services-post">
                            <a href="#"><i class="fa fa-magic"></i></a>
                            <h2>Web Developing</h2>
                            <p>Donec odio. Quisque volutpat mattis eros. Nullam malesuada </p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="services-post">
                            <a href="#"><i class="fa fa-gift"></i></a>
                            <h2>Providing Domain & Hosting</h2>
                            <p>Donec odio. Quisque volutpat mattis eros. Nullam malesuada </p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="services-post">
                            <a href="#"><i class="fa fa-diamond"></i></a>
                            <h2>Magento Design & Development</h2>
                            <p>Donec odio. Quisque volutpat mattis eros. Nullam malesuada </p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="services-post">
                            <a href="#"><i class="fa fa-wordpress"></i></a>
                            <h2>Wordpress Theme & Plugin Development</h2>
                            <p>Donec odio. Quisque volutpat mattis eros. Nullam malesuada </p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="services-post">
                            <a href="#"><i class="fa fa-forumbee"></i></a>
                            <h2>Support & Maintenance</h2>
                            <p>Donec odio. Quisque volutpat mattis eros. Nullam malesuada </p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="services-post">
                            <a href="#"><i class="fa fa-bicycle"></i></a>
                            <h2>Website Customisation</h2>
                            <p>Donec odio. Quisque volutpat mattis eros. Nullam malesuada </p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="services-post">
                            <a href="#"><i class="fa fa-foursquare"></i></a>
                            <h2>Social Media Ads</h2>
                            <p>Donec odio. Quisque volutpat mattis eros. Nullam malesuada </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Start Service Section -->
        
        
        <!-- Start Fun Facts Section -->
    <section class="fun-facts">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-3 col-md-3 wow fadeInLeft" data-wow-duration="2s" data-wow-delay="300ms">
                      <div class="counter-item">
                        <i class="fa fa-cloud-upload"></i>
                        <div class="timer" id="item1" data-to="991" data-speed="5000"></div>
                          <p>Sed ut perspiciatis unde omnis iste natus voluptatem accusantium laudantium aperiam.</p>
                        <h3>Files uploaded</h3>                               
                      </div>
                    </div>  
                    <div class="col-xs-12 col-sm-3 col-md-3 wow fadeInUp" data-wow-duration="2s" data-wow-delay="300ms">
                      <div class="counter-item">
                        <i class="fa fa-check"></i>
                        <div class="timer" id="item2" data-to="7394" data-speed="5000"></div>
                          <p>Sed ut perspiciatis unde omnis iste natus voluptatem accusantium laudantium aperiam.</p>
                        <h3>Projects completed</h3>                               
                      </div>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 wow fadeInDown" data-wow-duration="2s" data-wow-delay="300ms">
                      <div class="counter-item">
                        <i class="fa fa-code"></i>
                        <div class="timer" id="item3" data-to="18745" data-speed="5000"></div>
                          <p>Sed ut perspiciatis unde omnis iste natus voluptatem accusantium laudantium aperiam.</p>
                        <h3>Lines of code written</h3>                               
                      </div>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3 wow fadeInRight" data-wow-duration="2s" data-wow-delay="300ms">
                      <div class="counter-item">
                        <i class="fa fa-male"></i>
                        <div class="timer" id="item4" data-to="8423" data-speed="5000"></div>
                          <p>Sed ut perspiciatis unde omnis iste natus voluptatem accusantium laudantium aperiam.</p>
                        <h3>Happy clients</h3>                               
                      </div>
                    </div>
            </div>
        </div>
    </section>
    <!-- End Fun Facts Section -->
        
        
        <!-- Start Pricing Section -->
    <section id="pricing-section" class="pricing-section">
        <div class="container">
           <div class="row">
                <div class="col-md-12">
                    <div class="section-title text-center wow fadeInDown" data-wow-duration="2s" data-wow-delay="50ms">
                        <h2>Service Price</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="pricing">
                        <div class="pricing-header">
                            <i class="fa fa-bars"></i>
                        </div>
                        <div class="pricing-body">
                            <h3 class="pricing-title">Personal</h3>
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
                            <a href="#" class="btn btn-primary">$69 / Month</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="pricing">
                        <div class="pricing-header colored-bg">
                            <i class="fa fa-diamond"></i>
                        </div>
                        <div class="pricing-body">
                            <h3 class="pricing-title">Professional</h3>
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
                            <a href="#" class="btn btn-primary">$99 / Month</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="pricing">
                        <div class="pricing-header">
                            <i class="fa fa-trophy"></i>
                        </div>
                        <div class="pricing-body">
                            <h3 class="pricing-title">Business</h3>
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
                            <a href="#" class="btn btn-primary">$169 / Month</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Pricing Section -->
        
        
        
        <!-- Start Testimonial Section -->
        <section id="testimonial-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="testimonial-wrapper">
                            <div class="testimonial-item">
                                <h2>What People Say About Us</h2>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. </p>
                                <img src="assets/front/images/team/team.jpg" alt="Testimonial images">
                                <h5>John Doe</h5>
                                <div class="desgnation">CEO, ThemeBean</div>
                            </div>
                            <div class="testimonial-item">
                                <h2>What People Say About Us</h2>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                                <img src="assets/front/images/team/team4.jpg" alt="Testimonial images">
                                <h5>John Doe</h5>
                                <div class="desgnation">CEO, ThemeBean</div>
                            </div>
                            <div class="testimonial-item">
                                <h2>What People Say About Us</h2>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                                <img src="assets/front/images/team/teamm.jpg" alt="Testimonial images">
                                <h5>John Doe</h5>
                                <div class="desgnation">CEO, ThemeBean</div>
                            </div>
                            <div class="testimonial-item">
                                <h2>What People Say About Us</h2>
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                                <img src="assets/front/images/team/team2.jpg" alt="Testimonial images">
                                <h5>John Doe</h5>
                                <div class="desgnation">CEO, ThemeBean</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Testimonial Section -->
        
        
        <!-- Start Client Section -->
        <div id="client-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="client-box">
                            <ul class="client-list">
                                <li><a href="#"><img src="assets/front/images/clients/client1.png" class="img-responsive" alt="Clients Logo"></a></li>
                                <li><a href="#"><img src="assets/front/images/clients/client2.png" class="img-responsive" alt="Clients Logo"></a></li>
                                <li><a href="#"><img src="assets/front/images/clients/client3.png" class="img-responsive" alt="Clients Logo"></a></li>
                                <li><a href="#"><img src="assets/front/images/clients/client4.png" class="img-responsive" alt="Clients Logo"></a></li>
                                <li><a href="#"><img src="assets/front/images/clients/client5.png" class="img-responsive" alt="Clients Logo"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Client Section -->



<?php
include_once 'includes/footer.php';
?>