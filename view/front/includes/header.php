<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">

<head>

    <!-- Basic -->
    <title>Genuine MARS</title>

    <!-- Define Charset -->
    <meta charset="utf-8">

    <!-- Responsive Metatag -->
   <!-- <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">-->

    <!-- Page Description and Author -->
  <!-- <meta name="description" content="If You're Happy, We're Happy">-->
   <!-- <meta name="author" content="Shahriyar Ahmed">-->
    <base href="http://localhost/gm/">

    <!-- Bootstrap CSS  -->
    <link rel="stylesheet" href="assets/front/bootstrap/css/bootstrap.min.css" type="text/css">



    <!-- Font Awesome CSS -->
    <link rel="stylesheet" href="assets/front/font-awesome/css/font-awesome.min.css" type="text/css">

    <!-- Owl Carousel CSS -->
    <link rel="stylesheet" href="assets/front/css/owl.carousel.css" type="text/css">
    <link rel="stylesheet" href="assets/front/css/owl.theme.css" type="text/css">
    <link rel="stylesheet" href="assets/front/css/owl.transitions.css" type="text/css">

    <!-- Css3 Transitions Styles  -->
    <link rel="stylesheet" type="text/css" href="assets/front/css/animate.css">

    <!-- Lightbox CSS -->
    <link rel="stylesheet" type="text/css" href="assets/front/css/lightbox.css">

    <!-- Sulfur CSS Styles  -->
    <link rel="stylesheet" type="text/css" href="assets/front/css/style.css">

    <!-- Responsive CSS Style -->
    <link rel="stylesheet" type="text/css" href="assets/front/css/responsive.css">


    <script src="assets/front/js/modernizrr.js"></script>


</head>