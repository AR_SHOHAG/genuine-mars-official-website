<!-- Start Footer Section -->
<section id="footer-section" class="footer-section">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="section-heading-2">
                    <h3 class="section-title">
                        <span>Office Address</span>
                    </h3>
                </div>

                <div class="footer-address">
                    <ul>
                        <li class="footer-contact"><i class="fa fa-home"></i>Dhaka, Bangladesh</li>
                        <li class="footer-contact"><i class="fa fa-envelope"></i><a href="#">official@genuinemars.com</a></li>
                        <li class="footer-contact"><i class="fa fa-phone"></i>+880 1724338212</li>
                        <li class="footer-contact"><i class="fa fa-phone"></i>+880 1930551009</li>
                        <li class="footer-contact"><i class="fa fa-globe"></i><a href="#" target="_blank">https://genuinemars.com/</a></li>
                    </ul>
                </div>
            </div><!--/.col-md-3 -->


            <div class="col-md-3">
                <div class="section-heading-2">
                    <h3 class="section-title">
                        <span>Latest News</span>
                    </h3>
                </div>

                <div class="latest-tweet">
                    <div class="media">
                        <div class="media-left">
                            <i class="fa fa-twitter fa-2x media-object"></i>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Some days ago</h4>
                            <p>The new CEO is kicking ass. Check it out here <a href="#" >https://genuinemars.com/</p>
                        </div>
                    </div>
                </div>
            </div><!--/.col-md-3 -->

            <div class="col-md-3">
                <div class="section-heading-2">
                    <h3 class="section-title">
                        <span>Stay With us</span>
                    </h3>
                </div>
                <div class="subscription">
                    <p>Stay with us for more exciting works and innovations.</p>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Your E-mail" id="name" required data-validation-required-message="Please enter your name.">
                        <input type="submit" class="btn btn-primary" value="Subscribe">
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="section-heading-2">
                    <h3 class="section-title">
                        <span>FLICKR STREAM</span>
                    </h3>
                </div>

                <div class="flickr-widget">
                    <ul class="flickr-list">
                        <li>
                            <a href="assets/front/images/portfolio/img1.jpg" data-lightbox="picture-1">
                                <img src="assets/front/images/portfolio/img1.jpg" alt="" class="img-responsive">
                            </a>
                        </li>
                        <li>
                            <a href="assets/front/images/portfolio/img2.jpg" data-lightbox="picture-2">
                                <img src="assets/front/images/portfolio/img2.jpg" alt="" class="img-responsive">
                            </a>
                        </li>
                        <li>
                            <a href="assets/front/images/portfolio/img3.jpg" data-lightbox="picture-3">
                                <img src="assets/front/images/portfolio/img3.jpg" alt="" class="img-responsive">
                            </a>
                        </li>
                        <li>
                            <a href="assets/front/images/portfolio/img4.jpg" data-lightbox="picture-4">
                                <img src="assets/front/images/portfolio/img4.jpg" alt="" class="img-responsive">
                            </a>
                        </li>
                        <li>
                            <a href="assets/front/images/portfolio/img5.jpg" data-lightbox="picture-5">
                                <img src="assets/front/images/portfolio/img5.jpg" alt="" class="img-responsive">
                            </a>
                        </li>
                        <li>
                            <a href="assets/front/images/portfolio/img6.jpg" data-lightbox="picture-6">
                                <img src="assets/front/images/portfolio/img6.jpg" alt="" class="img-responsive">
                            </a>
                        </li>
                        <li>
                            <a href="assets/front/images/portfolio/img1.jpg" data-lightbox="picture-7">
                                <img src="assets/front/images/portfolio/img1.jpg" alt="" class="img-responsive">
                            </a>
                        </li>
                        <li>
                            <a href="assets/front/images/portfolio/img2.jpg" data-lightbox="picture-8">
                                <img src="assets/front/images/portfolio/img2.jpg" alt="" class="img-responsive">
                            </a>
                        </li>
                    </ul>
                </div>
            </div><!--/.col-md-3 -->
        </div><!--/.row -->
    </div><!-- /.container -->
</section>
<!-- End Footer Section -->


<!-- Start CCopyright Section -->
<div id="copyright-section" class="copyright-section">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <div class="copyright">
                    Copyright © 2017. All Contents Reserved by <a href="https://genuinemars.com/">Genuine MARS</a>
                </div>
            </div>

            <div class="col-md-5">
                <div class="copyright-menu pull-right">
                    <ul>
                        <li><a href="index.php" class="active">Home</a></li>
                        <li><a href="view/front/comming_soon.php">Sample Site</a></li>
                        <li><a href="https://genuinemars.com/">https://genuinemars.com/</a></li>
                    </ul>
                </div>
            </div>
        </div><!--/.row -->
    </div><!-- /.container -->
</div>
<!-- End CCopyright Section -->



<!-- Sulfur JS File -->
<script src="assets/front/js/jquery-2.1.3.min.js"></script>
<script src="assets/front/js/jquery-migrate-1.2.1.min.js"></script>
<script src="assets/front/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/front/js/owl.carousel.min.js"></script>
<script src="assets/front/js/jquery.appear.js"></script>
<script src="assets/front/js/jquery.fitvids.js"></script>
<script src="assets/front/js/jquery.nicescroll.min.js"></script>
<script src="assets/front/js/lightbox.min.js"></script>
<script src="assets/front/js/count-to.js"></script>
<script src="assets/front/js/styleswitcher.js"></script>

<script src="assets/front/js/map.js"></script>
<script src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
<script src="assets/front/js/script.js"></script>


</body>
</html>