<?php
include_once 'view/front/includes/header.php';
?>

    <body>

    <header class="clearfix">

        <!-- Start Top Bar -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="top-bar">
                        <div class="row">

                            <div class="col-md-6">
                                <!-- Start Contact Info -->
                                <ul class="contact-details">
                                    <li><a href="#"><i class="fa fa-phone"></i> +880 1724338212</a>
                                    </li>
                                    <li><a href="#"><i class="fa fa-envelope-o"></i>support@genuinemars.com</a>
                                    </li>
                                </ul>
                                <!-- End Contact Info -->
                            </div><!-- .col-md-6 -->

                            <div class="col-md-6">
                                <!-- Start Social Links -->
                                <ul class="social-list">
                                    <li>
                                        <a href="https://www.facebook.com/genuinemars/"><i class="fa fa-rss"></i></a>
                                    </li>
                                    <li>
                                        <a href="https://www.facebook.com/genuinemars/"><i class="fa fa-facebook"></i></a>
                                    </li>
                                    <li>
                                        <a href="https://www.facebook.com/genuinemars/"><i class="fa fa-twitter"></i></a>
                                    </li>
                                    <li>
                                        <a href="https://www.facebook.com/genuinemars/"><i class="fa fa-google-plus"></i></a>
                                    </li>
                                    <li>
                                        <a href="https://www.facebook.com/genuinemars/"><i class="fa fa-youtube"></i></a>
                                    </li>
                                    <li>
                                        <a href="https://www.facebook.com/genuinemars/"><i class="fa fa-linkedin"></i></a>
                                    </li>
                                    <li>
                                        <a href="https://www.facebook.com/genuinemars/"><i class="fa fa-flickr"></i></a>
                                    </li>
                                    <li>
                                        <a href="https://www.facebook.com/genuinemars/"><i class="fa fa-vimeo-square"></i></a>
                                    </li>
                                    <li>
                                        <a href="https://www.facebook.com/genuinemars/"><i class="fa fa-skype"></i></a>
                                    </li>
                                </ul>
                                <!-- End Social Links -->
                            </div><!-- .col-md-6 -->
                        </div>


                    </div>
                </div>

            </div><!-- .row -->
        </div><!-- .container -->
        <!-- End Top Bar -->

        <!-- Start  Logo & Naviagtion  -->
        <div class="navbar navbar-default navbar-top">
            <div class="container">
                <div class="navbar-header">
                    <!-- Stat Toggle Nav Link For Mobiles -->
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <i class="fa fa-bars"></i>
                    </button>
                    <!-- End Toggle Nav Link For Mobiles -->
                    <a class="navbar-brand" href="index.php">GENUINE MARS</a>
                </div>
                <div class="navbar-collapse collapse">

                    <!-- Start Navigation List -->
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a class="active" href="index.php">Home</a>
                        </li>
                        <li>
                            <a href="view/front/about.php">About Us</a>
                        </li>
                        <li>
                            <a href="view/front/service.php">Service</a>
                        </li>
                        <li>
                            <a href="view/front/portfolio.php">Portfolio</a>
                        </li>
                        <li>
                            <a href="view/front/blog.php">Blog</a>
                            <ul class="dropdown">
                                <li>
                                    <a href="view/front/blog-item.php">Item Page</a>
                                </li>
                            </ul>
                        </li>
                        <li><a href="view/front/contact.php">Contact</a>
                        </li>
                    </ul>
                    <!-- End Navigation List -->
                </div>
            </div>
        </div>
        <!-- End Header Logo & Naviagtion -->

    </header>


    <!-- Start Header Section -->
    <div class="banner">
        <div class="overlay">
            <div class="container">
                <div class="intro-text">
                    <h1>Welcome To <span>Genuine MARS</span></h1>
                    <p>We are creative people with innovative works <br> Love to take challenge</p>
                    <a href="view/front/about.php" class="page-scroll btn btn-primary">Read More</a>
                </div>
            </div>
        </div>
    </div>
    <!-- End Header Section -->


    <!-- Start About Us Section -->
    <section id="about-section" class="about-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title text-center wow fadeInDown" data-wow-duration="2s" data-wow-delay="50ms">
                        <h2>About Us</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-5">
                    <div class="about-img">
                        <img src="assets/front/images/ceo.jpg" class="img-responsive" alt="About images">
                        <!-- <div class="head-text">
                             <p>Genuine MARS is founded to create an invincible originality in IT/Software business in the web world.</p>
                             <span>CEO,  Md. Arifur Rahman</span>
                         </div>-->
                    </div>
                    <div class="head-text">
                        <p>"Genuine MARS is founded to create an invincible originality in IT/Software business in the web world."</p>
                        <span>CEO,  Md. Arifur Rahman</span>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="about-text">
                        <p>Genuine mars is a highly reputed software company that has been praised by the professional customers all over the world. We do things what we would love to do, and that makes us distinct from others. Besides we have the most highly skilled visionary creative developers those are deeply compatible with recent technologies.</p>
                        <p>We work on some sort of activities. At present we design and develop high quality responsive websites by the touch of our highly professional web team. We also develop wordpess template theme, plugins and sell them in our own marketplace as well as the popular marketplaces in the world. Besides we provide domain hosting for websites with very excellent customer service.</p>
                    </div>

                    <div class="about-list">
                        <h4>Some important Feature</h4>
                        <ul>
                            <li><i class="fa fa-check-square"></i>We are always here to help you any time you need.</li>
                            <li><i class="fa fa-check-square"></i>We always value our clients’ precious time so we never compromise with delivery time.</li>
                            <li><i class="fa fa-check-square"></i>We always make exclusive and attractive design.</li>
                            <li><i class="fa fa-check-square"></i>To make our clients’ tension free we always focus on highest security of our services.</li>
                            <li><i class="fa fa-check-square"></i>We always develop fully responsive and Mobile friendly website.</li>
                            <li><i class="fa fa-check-square"></i>Ready to make clients fully satisfied.</li>
                        </ul>

                        <h4>More Feature</h4>
                        <ul>
                            <li><i class="fa fa-check-square"></i>Lifetime Support.</li>
                            <li><i class="fa fa-check-square"></i>In Time Delivery</li>
                            <li><i class="fa fa-check-square"></i>Reasonable price.</li>
                            <li><i class="fa fa-check-square"></i>No compromise with quality.</li>
                        </ul>
                    </div>

                </div>



            </div>
        </div>
    </section>


    <!-- Start Call to Action Section -->
    <section class="call-to-action">
        <div class="container">
            <div class="row">
                <div class="col-md-12 wow zoomIn" data-wow-duration="2s" data-wow-delay="300ms">
                    <p>With  years of experience, being a part of an awesome team <br>we've created a creative  and aesthetic environment that <br>breeds innovative works. Please take a look.</p>
                </div>
            </div>
        </div>
    </section>
    <!-- End Call to Action Section -->




    <!-- Start Team Member Section -->
    <section id="team-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title text-center wow fadeInDown" data-wow-duration="2s" data-wow-delay="50ms">
                        <h2>Our Team</h2>
                        <p>Meet with the awesome team members</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 wow fadeInLeft" data-wow-duration="2s" data-wow-delay="300ms">
                    <div class="team-member">
                        <img src="assets/front/images/team/teamm.jpg" class="img-responsive" alt="">
                        <div class="team-details">
                            <h4>Md. Arifur Rahman</h4>
                            <p>Co-Founder & CEO</p>
                            <ul>
                                <li><a href="https://www.facebook.com/profile.php?id=100007331657504"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                                <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div><!-- /.col-md-3 -->
                <div class="col-md-3 wow fadeInLeft" data-wow-duration="2s" data-wow-delay="600ms">
                    <div class="team-member">
                        <img src="assets/front/images/team/team2.jpg" class="img-responsive" alt="">
                        <div class="team-details">
                            <h4>Al-Mahfuz</h4>
                            <p>Co-Founder & CEO</p>
                            <ul>
                                <li><a href="https://www.facebook.com/mahfuz380?ref=br_rs"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                                <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div><!-- /.col-md-3 -->
                <div class="col-md-3 wow fadeInLeft" data-wow-duration="2s" data-wow-delay="900ms">
                    <div class="team-member">
                        <img src="assets/front/images/team/team3.jpg" class="img-responsive" alt="">
                        <div class="team-details">
                            <h4>Md. Fayjur Rahmane</h4>
                            <p>Co-Founder & CEO</p>
                            <ul>
                                <li><a href="https://www.facebook.com/fayjur121"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                                <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div><!-- /.col-md-3 -->
                <div class="col-md-3 wow fadeInLeft" data-wow-duration="2s" data-wow-delay="1200ms">
                    <div class="team-member">
                        <img src="assets/front/images/team/team4.jpg" class="img-responsive" alt="">
                        <div class="team-details">
                            <h4>Sakib Sadman</h4>
                            <p>Graphic Designer</p>
                            <ul>
                                <li><a href="https://www.facebook.com/sakib.sadman.547"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                                <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div><!-- /.col-md-3 -->



            </div>

            <?php echo '<br>';?>

            <div class="row">
                <div class="col-md-3 wow fadeInLeft" data-wow-duration="2s" data-wow-delay="300ms">
                    <div class="team-member">
                        <img src="assets/front/images/team/team5.jpg" class="img-responsive" alt="">
                        <div class="team-details">
                            <h4>Sohel Rana</h4>
                            <p>Web Designer</p>
                            <ul>
                                <li><a href="https://www.facebook.com/profile.php?id=100007331657504"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                                <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div><!-- /.col-md-3 -->
                <div class="col-md-3 wow fadeInLeft" data-wow-duration="2s" data-wow-delay="600ms">
                    <div class="team-member">
                        <img src="assets/front/images/team/team6.jpg" class="img-responsive" alt="">
                        <div class="team-details">
                            <h4>Md. Shoriful Islam</h4>
                            <p>Graphic Designer</p>
                            <ul>
                                <li><a href="https://www.facebook.com/mahfuz380?ref=br_rs"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                                <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div><!-- /.col-md-3 -->
                <div class="col-md-3 wow fadeInLeft" data-wow-duration="2s" data-wow-delay="900ms">
                    <div class="team-member">
                        <img src="assets/front/images/team/team7.jpg" class="img-responsive" alt="">
                        <div class="team-details">
                            <h4>Wasiq Shibly</h4>
                            <p>Web Designer</p>
                            <ul>
                                <li><a href="https://www.facebook.com/fayjur121"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                                <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div><!-- /.col-md-3 -->
                <div class="col-md-3 wow fadeInLeft" data-wow-duration="2s" data-wow-delay="1200ms">
                    <div class="team-member">
                        <img src="assets/front/images/team/team8.jpg" class="img-responsive" alt="">
                        <div class="team-details">
                            <h4>Zakarea Khandakar</h4>
                            <p>Marketing Officer</p>
                            <ul>
                                <li><a href="https://www.facebook.com/sakib.sadman.547"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                                <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div><!-- /.col-md-3 -->



            </div>
        </div>
    </section>
    <!-- End Team Member Section -->


    <!-- Start Portfolio Section -->
    <section id="portfolio" class="portfolio-section-1">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title text-center wow fadeInDown" data-wow-duration="2s" data-wow-delay="50ms">
                        <h2>Our Portfolio</h2>
                        <p>Here are a few things we've done. Stay with us and check out</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">

                    <!-- Start Portfolio items -->
                    <ul id="portfolio-list">
                        <li class="wow fadeInLeft" data-wow-duration="2s" data-wow-delay="300ms">
                            <div class="portfolio-item">
                                <img src="assets/front/images/portfolio/img%20(1).jpg" class="img-responsive" alt="" />
                                <div class="portfolio-caption">
                                    <h4>Portfolio Title</h4>
                                    <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium</p>
                                    <a href="#portfolio-modal" data-toggle="modal" class="link-1"><i class="fa fa-magic"></i></a>
                                    <a href="#" class="link-2"><i class="fa fa-link"></i></a>
                                </div>
                            </div>
                        </li>
                        <li class="wow fadeInLeft" data-wow-duration="2s" data-wow-delay="600ms">
                            <div class="portfolio-item">
                                <img src="assets/front/images/portfolio/img%20(2).jpg" class="img-responsive" alt="" />
                                <div class="portfolio-caption">
                                    <h4>Portfolio Title</h4>
                                    <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium</p>
                                    <a href="#portfolio-modal" data-toggle="modal" class="link-1"><i class="fa fa-magic"></i></a>
                                    <a href="#" class="link-2"><i class="fa fa-link"></i></a>
                                </div>
                            </div>
                        </li>
                        <li class="wow fadeInLeft" data-wow-duration="2s" data-wow-delay="900ms">
                            <div class="portfolio-item">
                                <img src="assets/front/images/portfolio/img%20(3).jpg" class="img-responsive" alt="" />
                                <div class="portfolio-caption">
                                    <h4>Portfolio Title</h4>
                                    <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium</p>
                                    <a href="#portfolio-modal" data-toggle="modal" class="link-1"><i class="fa fa-magic"></i></a>
                                    <a href="#" class="link-2"><i class="fa fa-link"></i></a>
                                </div>
                            </div>
                        </li>
                        <li class="wow fadeInLeft" data-wow-duration="2s" data-wow-delay="1200ms">
                            <div class="portfolio-item">
                                <img src="assets/front/images/portfolio/img%20(4).jpg" class="img-responsive" alt="" />
                                <div class="portfolio-caption">
                                    <h4>Portfolio Title</h4>
                                    <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium</p>
                                    <a href="#portfolio-modal" data-toggle="modal" class="link-1"><i class="fa fa-magic"></i></a>
                                    <a href="#" class="link-2"><i class="fa fa-link"></i></a>
                                </div>
                            </div>
                        </li>
                        <li class="wow fadeInLeft" data-wow-duration="2s" data-wow-delay="1500ms">
                            <div class="portfolio-item">
                                <img src="assets/front/images/portfolio/img%20(5).jpg" class="img-responsive" alt="" />
                                <div class="portfolio-caption">
                                    <h4>Portfolio Title</h4>
                                    <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium</p>
                                    <a href="#portfolio-modal" data-toggle="modal" class="link-1"><i class="fa fa-magic"></i></a>
                                    <a href="#" class="link-2"><i class="fa fa-link"></i></a>
                                </div>
                            </div>
                        </li>
                        <li class="wow fadeInLeft" data-wow-duration="2s" data-wow-delay="1800ms">
                            <div class="portfolio-item">
                                <img src="assets/front/images/portfolio/img%20(6).jpg" class="img-responsive" alt="" />
                                <div class="portfolio-caption">
                                    <h4>Portfolio Title</h4>
                                    <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium</p>
                                    <a href="#portfolio-modal" data-toggle="modal" class="link-1"><i class="fa fa-magic"></i></a>
                                    <a href="#" class="link-2"><i class="fa fa-link"></i></a>
                                </div>
                            </div>
                        </li>


                    </ul>
                    <!-- End Portfolio items -->
                </div>
            </div>
        </div>
    </section>
    <!-- End Portfolio Section -->


    <!-- Start Service Section -->
    <section id="service-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title text-center wow fadeInDown" data-wow-duration="2s" data-wow-delay="50ms">
                        <h2>Our Services</h2>
                        <p>We are the only one who provide the best quality <br>products at the cheapest price in the global market. <br>We believe our price strategy is the best you've ever seen.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="services-post">
                        <a href="#"><i class="fa fa-skyatlas"></i></a>
                        <h2>RESPONSIVE DESIGN</h2>
                        <p>Donec odio. Quisque volutpat mattis eros. Nullam malesuada </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="services-post">
                        <a href="#"><i class="fa fa-magic"></i></a>
                        <h2>Web Developing</h2>
                        <p>Donec odio. Quisque volutpat mattis eros. Nullam malesuada </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="services-post">
                        <a href="#"><i class="fa fa-gift"></i></a>
                        <h2>Providing Domain & Hosting</h2>
                        <p>Donec odio. Quisque volutpat mattis eros. Nullam malesuada </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="services-post">
                        <a href="#"><i class="fa fa-diamond"></i></a>
                        <h2>Magento Design & Development</h2>
                        <p>Donec odio. Quisque volutpat mattis eros. Nullam malesuada </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="services-post">
                        <a href="#"><i class="fa fa-wordpress"></i></a>
                        <h2>Wordpress Theme & Plugin Development</h2>
                        <p>Donec odio. Quisque volutpat mattis eros. Nullam malesuada </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="services-post">
                        <a href="#"><i class="fa fa-forumbee"></i></a>
                        <h2>Support & Maintenance</h2>
                        <p>Donec odio. Quisque volutpat mattis eros. Nullam malesuada </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="services-post">
                        <a href="#"><i class="fa fa-bicycle"></i></a>
                        <h2>Website Customisation</h2>
                        <p>Donec odio. Quisque volutpat mattis eros. Nullam malesuada </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="services-post">
                        <a href="#"><i class="fa fa-foursquare"></i></a>
                        <h2>Social Media Ads</h2>
                        <p>Donec odio. Quisque volutpat mattis eros. Nullam malesuada </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Start Service Section -->



    <!-- Start Testimonial Section -->
    <section id="testimonial-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="testimonial-wrapper">
                        <div class="testimonial-item">
                            <h2>What People Say About Us</h2>
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. </p>
                            <img src="assets/front/images/team/team.jpg" alt="Testimonial images">
                            <h5>John Doe</h5>
                            <div class="desgnation">CEO, ThemeBean</div>
                        </div>
                        <div class="testimonial-item">
                            <h2>What People Say About Us</h2>
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                            <img src="assets/front/images/team/team4.jpg" alt="Testimonial images">
                            <h5>John Doe</h5>
                            <div class="desgnation">CEO, ThemeBean</div>
                        </div>
                        <div class="testimonial-item">
                            <h2>What People Say About Us</h2>
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                            <img src="assets/front/images/team/teamm.jpg" alt="Testimonial images">
                            <h5>John Doe</h5>
                            <div class="desgnation">CEO, ThemeBean</div>
                        </div>
                        <div class="testimonial-item">
                            <h2>What People Say About Us</h2>
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                            <img src="assets/front/images/team/team2.jpg" alt="Testimonial images">
                            <h5>John Doe</h5>
                            <div class="desgnation">CEO, ThemeBean</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Testimonial Section -->


    <!-- Start Client Section -->
    <div id="client-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="client-box">
                        <ul class="client-list">
                            <li><a href="#"><img src="assets/front/images/clients/client1.png" class="img-responsive" alt="Clients Logo"></a></li>
                            <li><a href="#"><img src="assets/front/images/clients/client2.png" class="img-responsive" alt="Clients Logo"></a></li>
                            <li><a href="#"><img src="assets/front/images/clients/client3.png" class="img-responsive" alt="Clients Logo"></a></li>
                            <li><a href="#"><img src="assets/front/images/clients/client4.png" class="img-responsive" alt="Clients Logo"></a></li>
                            <li><a href="#"><img src="assets/front/images/clients/client5.png" class="img-responsive" alt="Clients Logo"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Client Section -->



<?php
include_once 'view/front/includes/footer.php';
?>